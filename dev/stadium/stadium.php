<?php
//base on the info in http://brun.dk/hattrick/index.php?content=17&lang=english
error_reporting(E_NONE); // warnings or notices makes AJAX feel bad :)
//include needed files
require_once('common.php');

//get data from the user
$userInfo = array();
$userInfo['fans'] = str2int($_GET['fans']);
$userInfo['mood'] = str2int($_GET['mood']);

//check for the mood related array
foreach($stadium_table as $current){
	if($userInfo['mood']==str2int($current['mood'])){
		$stadium_array=$current;
		break;
	}
}

//the formula: number of fans * multiplier * percent 
$output = array();
$output['size'] = intval($userInfo['fans']*$stadium_array['multiply']);
$output['terraces'] =floor($userInfo['fans']*$stadium_array['multiply']*($stadium_array['terraces']/100));
$output['basic'] =floor($userInfo['fans']*$stadium_array['multiply']*($stadium_array['basic']/100));
$output['roof'] =floor($userInfo['fans']*$stadium_array['multiply']*($stadium_array['roof']/100));
$output['vip'] =floor($userInfo['fans']*$stadium_array['multiply']*($stadium_array['vip']/100));

if ($userInfo['fans'] > 5000)
	echo('<p class="red">Aţi introdus prea mulţi fani</p>');
else
	require_once('stadium.inc.html');
?>
