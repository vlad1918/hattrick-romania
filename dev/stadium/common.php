<?php
$stadium_table = array(
0 => array('mood' => '1', 'terraces' => '68.10', 'basic' => '20.30', 'roof' => '10.70', 'vip' => '0.90', 'multiply' => '11.53'),
1 => array('mood' => '2', 'terraces' => '64.28', 'basic' => '20.85', 'roof' => '13.51', 'vip' => '1.36', 'multiply' => '13.09'),
2 => array('mood' => '3', 'terraces' => '62.09', 'basic' => '21.16', 'roof' => '15.12', 'vip' => '1.64', 'multiply' => '14.63'),
3 => array('mood' => '4', 'terraces' => '60.55', 'basic' => '21.37', 'roof' => '16.25', 'vip' => '1.83', 'multiply' => '16.17'),
4 => array('mood' => '5', 'terraces' => '59.38', 'basic' => '21.52', 'roof' => '17.12', 'vip' => '1.98', 'multiply' => '17.72'),
5 => array('mood' => '6', 'terraces' => '58.43', 'basic' => '21.64', 'roof' => '17.83', 'vip' => '2.11', 'multiply' => '19.27'),
6 => array('mood' => '7', 'terraces' => '57.62', 'basic' => '21.73', 'roof' => '18.44', 'vip' => '2.21', 'multiply' => '20.83'),
7 => array('mood' => '8', 'terraces' => '56.93', 'basic' => '21.80', 'roof' => '18.97', 'vip' => '2.30', 'multiply' => '22.39'),
8 => array('mood' => '9', 'terraces' => '56.32', 'basic' => '21.86', 'roof' => '19.44', 'vip' => '2.37', 'multiply' => '23.95'),
9 => array('mood' => '10', 'terraces' => '55.78', 'basic' => '21.92', 'roof' => '19.86', 'vip' => '2.44', 'multiply' => '25.52'),
10 => array('mood' => '11', 'terraces' => '55.29', 'basic' => '21.96', 'roof' => '20.24', 'vip' => '2.50', 'multiply' => '27.09')
);

//converts string to integer
function str2int($string, $concat = true) {
    $length = strlen($string);    
    for ($i = 0, $int = '', $concat_flag = true; $i < $length; $i++) {
        if (is_numeric($string[$i]) && $concat_flag) {
            $int .= $string[$i];
        } elseif(!$concat && $concat_flag && strlen($int) > 0) {
            $concat_flag = false;
        }        
    }
    
    return (int) $int;
}
?>
