<?php
error_reporting(E_NONE); // warnings or notices makes AJAX feel bad :)

//get data from the user
$userInfo = array();
$userInfo['bruised'] = intval($_GET['bruised']);
$userInfo['weeks'] = intval($_GET['weeks']);
$userInfo['age'] = intval($_GET['age']);
$userInfo['doctors'] = intval($_GET['doctors']);

if ($userInfo['doctors'] < 0 || $userInfo['doctors'] > 255)
	$error = 'Numărul doctorilor trebuie să fie între 0 şi 255';

if ($userInfo['weeks'] < 0 || $userInfo['weeks'] > 20)
	$error = 'Nu se poate face calculul pentru atâtea săptămâni';

$updates = ceil(5.3875 / pow(($userInfo['doctors'] + 1), 0.2155) / (1-($userInfo['age']-17)*0.041));

// Adjustments for very old players

if ($userInfo['age'] >= 30 && $userInfo['age'] < 33 )
	$updates = $updates + 1;
if ($userInfo['age'] >= 33 && $userInfo['age'] < 37 )
	$updates = $updates + 2;

$bruised = $updates; // Updates for recovering form bruised but playing

if ($userInfo['bruised'] == 0) // for injuries
	$injured = $userInfo['weeks'] * $updates;

//Generate the response HTML code
require_once('healing.inc.html');

?>
