function GetXmlHttpObject()
{
if (window.XMLHttpRequest)
  {
  // code for IE7+, Firefox, Chrome, Opera, Safari
  return new XMLHttpRequest();
  }
if (window.ActiveXObject)
  {
  // code for IE6, IE5
  return new ActiveXObject("Microsoft.XMLHTTP");
  }
return null;
}

function heal(bruised, weeks, age, doctors)
{

	if (bruised == true)
		bruised = 1;
	else
		bruised = 0;

	xmlhttp = GetXmlHttpObject(); //xmlhttp object
	if (xmlhttp==null)
	{
		alert ("Your browser does not support XMLHTTP!");
		return;
	} 
	var url = "dev/healing/healing.php"
	var params = "bruised="+bruised+"&weeks="+weeks+"&age="+age+"&doctors="+doctors;
	xmlhttp.onreadystatechange=stateChanged;
	xmlhttp.open("GET", url+"?"+params, true);
	xmlhttp.send(null);
	
}

function changeType(checked)
{

	if (checked == true)
		document.getElementById('weeks').disabled=true;
	else
		document.getElementById('weeks').disabled=false;
}

function stateChanged()
{
if (xmlhttp.readyState==4)
  {
  document.getElementById("results").innerHTML=xmlhttp.responseText;
  }
}
