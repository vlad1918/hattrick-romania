<?php
// Functions are defined in here becouse you can't include files in the PHP Joomla! component.

/**
 * @param int $ageInDays
 * @return int $htAge['years']
 * @return int $htAge['days']
 */
function calcAge($ageInDays)
{
	$htAge['days'] = $ageInDays % 112;
	$htAge['years'] = 15 + ($ageInDays - $htAge['days'])/112;
	
	return $htAge;
}
	

/**
 * @param int $startdate
 * @return array $championship
 */
function championshipTable($start) 
{
	$day = 24*60*60; // Number of seconds in a day
	/*** Read this!!! ***/
	// All dates are referenced by the UNIX Timestamp!
	// A player ages are referenced in DAYS.
	// A player that is 0 days old is 15 years and 0 days old in Hattrick
	// A player that is 112 days old is 16 years and 0 days old in Hattrick	
	// A player that is 224 days old is 17 years and 0 days old in Hattrick	
	// A player that is 336 days old is 18 years and 0 days old in Hattrick	
	// A player that is 448 days old is 19 years and 0 days old in Hattrick	
	// A player that is 560 days old is 20 years and 0 days old in Hattrick
	$time2string = getdate();
	$today = strtotime($time2string['year'].'-'.$time2string['mon'].'-'.$time2string['mday'].' 12:00');
	$dif = floor(($today - $start)/$day); // How many days have passed since the start of the season
	$blessedDay = 560 + $dif + 111;
	$calendar = array();
	$calendar[0]['name'] = 'Meci1';
	$calendar[0]['date'] = $start;
	$age = calcAge($blessedDay);
	$calendar[0]['years'] = $age['years'];
	$calendar[0]['days'] = $age['days'];
	//Qualifiers
	for ($i=1; $i<14; $i++)
	{
		$calendar[$i]['name'] = 'Meci'.($i+1);
		$calendar[$i]['date'] = $calendar[$i-1]['date'] + 7*$day;
		$blessedDay = $blessedDay - 7;
		$age = calcAge($blessedDay);
		$calendar[$i]['years'] = $age['years'];
		$calendar[$i]['days'] = $age['days'];
	}
	//Round 1
	$calendar[14]['name'] = 'Meci1-1';
	$calendar[14]['date'] = $calendar[13]['date'] + 56*$day;
	$blessedDay = $blessedDay - 56;
	$age = calcAge($blessedDay);
	$calendar[14]['years'] = $age['years'];
	$calendar[14]['days'] = $age['days'];

	$calendar[15]['name'] = 'Meci1-2';
	$calendar[15]['date'] = $calendar[14]['date'] + 3*$day;	
	$blessedDay = $blessedDay - 3;
	$age = calcAge($blessedDay);
	$calendar[15]['years'] = $age['years'];
	$calendar[15]['days'] = $age['days'];

	$calendar[16]['name'] = 'Meci1-3';
	$calendar[16]['date'] = $calendar[15]['date'] + 4*$day;
	$blessedDay = $blessedDay - 4;
	$age = calcAge($blessedDay);
	$calendar[16]['years'] = $age['years'];
	$calendar[16]['days'] = $age['days'];

	//Round 2
	$calendar[17]['name'] = 'Meci2-1';
	$calendar[17]['date'] = $calendar[16]['date'] + 28*$day;
	$blessedDay = $blessedDay - 28;
	$age = calcAge($blessedDay);
	$calendar[17]['years'] = $age['years'];
	$calendar[17]['days'] = $age['days'];

	$calendar[18]['name'] = 'Meci2-2';
	$calendar[18]['date'] = $calendar[17]['date'] + 3*$day;
	$blessedDay = $blessedDay - 3;
	$age = calcAge($blessedDay);
	$calendar[18]['years'] = $age['years'];
	$calendar[18]['days'] = $age['days'];

	$calendar[19]['name'] = 'Meci2-3';
	$calendar[19]['date'] = $calendar[18]['date'] + 4*$day;
	$blessedDay = $blessedDay - 4;
	$age = calcAge($blessedDay);
	$calendar[19]['years'] = $age['years'];
	$calendar[19]['days'] = $age['days'];

	//Round 3
	$calendar[20]['name'] = 'Meci3-1';
	$calendar[20]['date'] = $calendar[19]['date'] + 3*$day;
	$blessedDay = $blessedDay - 3;
	$age = calcAge($blessedDay);
	$calendar[20]['years'] = $age['years'];
	$calendar[20]['days'] = $age['days'];

	$calendar[21]['name'] = 'Meci3-2';
	$calendar[21]['date'] = $calendar[20]['date'] + 4*$day;
	$blessedDay = $blessedDay - 4;
	$age = calcAge($blessedDay);
	$calendar[21]['years'] = $age['years'];
	$calendar[21]['days'] = $age['days'];

	$calendar[22]['name'] = 'Meci3-3';
	$calendar[22]['date'] = $calendar[21]['date'] + 3*$day;
	$blessedDay = $blessedDay - 3;
	$age = calcAge($blessedDay);
	$calendar[22]['years'] = $age['years'];
	$calendar[22]['days'] = $age['days'];

	//Semifinals
	$calendar[23]['name'] = 'Semifinala';
	$calendar[23]['date'] = $calendar[22]['date'] + 4*$day;
	$blessedDay = $blessedDay - 4;
	$age = calcAge($blessedDay);
	$calendar[23]['years'] = $age['years'];
	$calendar[23]['days'] = $age['days'];

	//Finals
	$calendar[24]['name'] = 'Finala';
	$calendar[24]['date'] = $calendar[23]['date'] + 2*$day;
	$blessedDay = $blessedDay - 2;
	$age = calcAge($blessedDay);
	$calendar[24]['years'] = $age['years'];
	$calendar[24]['days'] = $age['days'];

return $calendar;
	
}


// Script starts here

$format = 'Y-m-d'; //The date format
//	$start = strtotime('2009-06-05 12:00'); // Season 37
	$start = strtotime('2010-01-15 12:00'); // Season 39
//	$start = strtotime('2010-08-27 12:00'); // Season 41
$calendar = championshipTable($start);

?>
<table style="border: 1px solid black; border-collapse: collapse; margin: 10px auto 10px;">
	<?php foreach($calendar as $nb => $row) { ?>
	<tr>
		<td style="border: 1px solid black; padding: 3px;"><?php echo($nb); ?></td>
		<td style="border: 1px solid black; padding: 3px;"><?php echo($row['name']); ?></td>
		<td style="border: 1px solid black; padding: 3px;"><?php echo(date($format, $row['date'])); ?></td>
		<td style="border: 1px solid black; padding: 3px;"><?php echo($row['years']); ?> ani si <?php echo($row['days']); ?> zile</td>
	</tr>
	<?php } ?>
</table>

<?php
$start = strtotime('2010-08-27 12:00'); // Season 41
$calendar = championshipTable($start);

?>
<table style="border: 1px solid black; border-collapse: collapse; margin: 10px auto 10px;">
	<?php foreach($calendar as $nb => $row) { ?>
	<tr>
		<td style="border: 1px solid black; padding: 3px;"><?php echo($nb); ?></td>
		<td style="border: 1px solid black; padding: 3px;"><?php echo($row['name']); ?></td>
		<td style="border: 1px solid black; padding: 3px;"><?php echo(date($format, $row['date'])); ?></td>
		<td style="border: 1px solid black; padding: 3px;"><?php echo($row['years']); ?> ani si <?php echo($row['days']); ?> zile</td>
	</tr>
	<?php } ?>
</table>
