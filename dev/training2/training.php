<?php
error_reporting(E_NONE); // warnings or notices makes AJAX feel bad :)
//include needed files
require_once('flattermanPredictor.class.php');
require_once('common.php');

//get data from the user
$userInfo = array();
$userInfo['age'] = intval($_GET['age']);
$userInfo['days'] = intval($_GET['days']);
$userInfo['skill'] = intval($_GET['skill']);
$userInfo['subSkill'] = intval($_GET['subSkill']);
if ($userInfo['subSkill'] > 0)
	$userInfo['skill'] = floatval($userInfo['skill'] + $userInfo['subSkill'] / 100);	
$userInfo['coach'] = floatval($_GET['coach']);
$userInfo['assistants'] = intval($_GET['assistants']);
$userInfo['intensity'] = intval($_GET['intensity']);
$userInfo['staminaShare'] = intval($_GET['staminaShare']);
$userInfo['position'] = $_GET['position'];

//the simulator
$predict = array();
$duration = 0; // counter for how long a player needs to train in order to pop (calculated in weeks)
$week = 0; // counter for the number of weeks since the start of the simulation
//$userInfo['surplusDays'] = $userInfo['days']; // add this value after the simulation. This are the surplus days (the original player days from his age ) inserted by the user in the days input
if ( $userInfo['age']>=17 && $userInfo['age']<=30 ) //If a correct age was given do the math
{
	do
	{
		$duration++; // one week was simulated
		$week++; // one week was simulated
		$player = new flattermanPredictor($userInfo['age'], $userInfo['days'], $userInfo['skill'], $userInfo['subSkill'], $userInfo['coach'], $userInfo['assistants'], $userInfo['intensity'], $userInfo['staminaShare'], $position[$userInfo['position']] );
		$trainingFactor = $player->trainingFactor();
		
		$userInfo['days'] = $userInfo['days'] + 7;
		if( $userInfo['days'] >= 112) // Player has aged 1 year
		{
			$userInfo['age'] = $userInfo['age'] + 1;
			$userInfo['days'] = 0; 	
		}
		
		$oldSkill = $userInfo['skill'];
		$userInfo['skill'] = $userInfo['skill'] + $trainingFactor;
		if( floor($oldSkill) < floor($userInfo['skill']) )
		{
			$userInfo['duration'] = $duration;
			$userInfo['week'] = $week;
//			$userInfo['days'] = $userInfo['days'] + $userInfo['surplusDays']; // the days from the player age inputs
			$predict[] = $userInfo; 
			$duration = 0; // reset duration since player has poped in skill
		}
		
	}
	while($userInfo['skill'] <= 20 && $userInfo['age'] <= 30);
}

require_once('training.inc.html');

?>
