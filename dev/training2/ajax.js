function GetXmlHttpObject()
{
if (window.XMLHttpRequest)
  {
  // code for IE7+, Firefox, Chrome, Opera, Safari
  return new XMLHttpRequest();
  }
if (window.ActiveXObject)
  {
  // code for IE6, IE5
  return new ActiveXObject("Microsoft.XMLHTTP");
  }
return null;
}

function simulate(age, days, position, skill, subSkill, coach, assistants, intensity, staminaShare)
{
/*	age = document.getElementById('age').value;
	days = document.getElementById('days').value;
	position = document.getElementById('position').value;
	skill = document.getElementById('skill').value;
	subSkill = document.getElementById('subSkill').value;
	coach = document.getElementById('coach').value;
	assistants = document.getElementById('assistants').value;
	intensity = document.getElementById('intensity').value;
	staminaShare = document.getElementById('staminaShare').value;	
*/	
	//alert(skill);
	xmlhttp = GetXmlHttpObject(); //xmlhttp object
	if (xmlhttp==null)
	{
		alert ("Your browser does not support XMLHTTP!");
		return;
	} 
	var url = "dev/training2/training.php"
	var params = "age="+age+"&days="+days+"&position="+position+"&skill="+skill+"&subSkill="+subSkill+"&coach="+coach+"&assistants="+assistants+"&intensity="+intensity+"&staminaShare="+staminaShare;
	xmlhttp.onreadystatechange=stateChanged;
	xmlhttp.open("GET", url+"?"+params, true);
	xmlhttp.send(null);
	
}

function stateChanged()
{
if (xmlhttp.readyState==4)
  {
  document.getElementById("results").innerHTML=xmlhttp.responseText;
  }
}
