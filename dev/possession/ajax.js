function GetXmlHttpObject()
{
if (window.XMLHttpRequest)
  {
  // code for IE7+, Firefox, Chrome, Opera, Safari
  return new XMLHttpRequest();
  }
if (window.ActiveXObject)
  {
  // code for IE6, IE5
  return new ActiveXObject("Microsoft.XMLHTTP");
  }
return null;
}

function calculatePossession(skill1, subSkill1, skill2, subSkill2)
{
	//alert(skill);

	xmlhttp = GetXmlHttpObject(); //xmlhttp object
	if (xmlhttp==null)
	{
		alert ("Your browser does not support XMLHTTP!");
		return;
	} 
	var url = "dev/possession/possession.php"
	var params = "skill1="+skill1+"&subSkill1="+subSkill1+"&skill2="+skill2+"&subSkill2="+subSkill2;
	xmlhttp.onreadystatechange=stateChanged;
	xmlhttp.open("GET", url+"?"+params, true);
	xmlhttp.send(null);
	
}

function stateChanged()
{
if (xmlhttp.readyState==4)
  {
  document.getElementById("results").innerHTML=xmlhttp.responseText;
  }
}
