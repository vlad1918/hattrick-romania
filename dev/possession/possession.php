<?php
error_reporting(E_NONE); // warnings or notices makes AJAX feel bad :)

//get data from the user
$userInfo = array();
$userInfo['skill1'] = intval($_GET['skill1']);
$userInfo['subSkill1'] = floatval($_GET['subSkill1']);
$userInfo['skill2'] = intval($_GET['skill2']);
$userInfo['subSkill2'] = floatval($_GET['subSkill2']);

//We only calculate the rating for one team! The other teams ratings are optained by substraction form 100%
$mid1 = $userInfo['skill1'] + $userInfo['subSkill1'];
$mid2 = $userInfo['skill2'] + $userInfo['subSkill2'];

$possession1 = floor(($mid1 / ($mid1 + $mid2) * 100));
$possession2 = 100 - $possession1;

//Generate the response HTML code
require_once('possession.inc.html');

?>
