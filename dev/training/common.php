<?php
$skill = array(
0 => array('ro' => 'inexistent', 'en' => 'non-existent'),
1 => array('ro' => 'dezastruos', 'en' => 'disatrous'),
2 => array('ro' => 'mizerabil', 'en' => 'wretched'),
3 => array('ro' => 'jalnic', 'en' => 'poor' ),
4 => array('ro' => 'slab', 'en' => 'weak'),
5 => array('ro' => 'inadecvat', 'en' => 'inadequate'),
6 => array('ro' => 'acceptabil', 'en' => 'passable'),
7 => array('ro' => 'bun', 'en' => 'solid'),
8 => array('ro' => 'excelent', 'en' => 'excellent'),
9 => array('ro' => 'formidabil', 'en' => 'formidable'),
10 => array('ro' => 'impresionant', 'en' => 'outstanding'),
11 => array('ro' => 'sclipitor', 'en' => 'brilliant'),
12 => array('ro' => 'magnific', 'en' => 'magnificent'),
13 => array('ro' => 'legendar', 'en' => 'worlclass'),
14 => array('ro' => 'supranatural', 'en' => 'supernatural'),
15 => array('ro' => 'titanic', 'en' => 'titanic'),
16 => array('ro' => 'extraterestru', 'en' => 'extra-terrestrial'),
17 => array('ro' => 'mitic', 'en' => 'mythical'),
18 => array('ro' => 'magic', 'en' => 'magical'),
19 => array('ro' => 'utopic', 'en' => 'utopian'),
20 => array('ro' => 'divin', 'en' => 'divine')
);

$position = array(
'DP' => '10.96',
'DF' => '5.48',
'GK' => '3.02',
'PM' => '4.66',
'SC' => '4.85',
'SP' => '0.99',
'SH' => '8.09',
'SPS' => '4.30',
'TPS' => '5.06',
'WA' => '5.39',
'WN' => '3.23',
);
?>
