<?php
class flattermanPredictor{
	
	var $age;
	var $days;
	var $skill;
	var $subSkill;
	var $coach;
	var $assistants;
	var $intensity;
	var $staminaShare;
	var $position;
	
	function __construct($age, $days, $skill, $subSkill, $coach, $assistants, $intensity, $staminaShare, $position)
	{
		$this->age = $age;
		$this->days = $days;
		$this->skill = $skill;
		$this->subSkill = $subSkill;
		$this->coach = $coach;
		$this->assistants = $assistants;
		$this->intensity = $intensity;
		$this->staminaShare = $staminaShare;
		$this->position = $position;

	}
	
	function ageFactor()
	{
		return pow(1.0404, ($this->age - 17) );
	}
	
	function skillFactor()
	{
		return -1.4595 * pow(( ($this->skill + 1) / 20), 2) + 3.7535 * ( ($this->skill +1 ) / 20) - 0.1349;
	}

	function coachFactor()
	{
		return 1 + (7 - $this->coach) * 0.0910;
	}
	
	function assistantsFactor()
	{
		return 1 + (log10(11) - log10($this->assistants + 1)) * 0.2749;
	}
	
	function intensityFactor()
	{
		return 100 / $this->intensity;
	}
	
	function staminaShareFactor()
	{
		return 1 / (1 - $this->staminaShare / 100);
	}
	
	function trainingFactor()
	{
		$trainingFactor = $this->position * $this->ageFactor() * $this->skillFactor() * $this->coachFactor() * $this->assistantsFactor() * $this->intensityFactor() * $this->staminaShareFactor();
		
		return 1/$trainingFactor;

	} 
		
}			
?>
