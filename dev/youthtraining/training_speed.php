<?
error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set("display_errors","1");

include('speed_values.php');

$skill[3]="Constructie";
$skill[4]="Extrema";
$skill[5]="Atac";
$skill[6]="Portar";
$skill[7]="Pase";
$skill[8]="Aparare";
$skill[9]="Faze Fixe";

$position[0]="Portar";
$position[1]="Fundas Central";
$position[2]="Fundas Lateral";
$position[3]="Extrema";
$position[4]="Mijlocas Central";
$position[5]="Atacant";

$training[0]="Faze Fixe";
$training[1]="Aparare";
$training[2]="Atac";
$training[3]="Extrema";
$training[4]="Sut";
$training[5]="Pase Scurte";
$training[6]="Constructie";
$training[7]="Portar";
$training[8]="Pase in Adancime";
$training[9]="Pozitionare Defensiva";
$training[10]="Atacuri pe Extrema";

$color[0]='#c9ffc9';
$color[1]='#ddffdd';

if ($_GET['training']!='' && $_GET['position']!='')
{
foreach ($spedd[$_GET['training']][$_GET['position']] as $key => $skill_speed)
{
?>
<h2>Crestere <?=$skill[$key]?></h2>
<br>
<table cellspacing=0 style="border-bottom: 1px solid #333333; border-right: 1px solid #333333;">
<tr bgcolor=#99ff99><td bgcolor=#ffffff></td><td colspan=9 align=center style="padding: 8px; border-top: 1px solid #333333; border-left: 1px solid #333333;">Treapta Crestere</td></tr>
<tr bgcolor=#99ff99><td bgcolor=#ffffff></td>
<td align=center style="padding: 8px; border-top: 1px solid #333333; border-left: 1px solid #333333;">0->1</td>
<td align=center style="padding: 8px; border-top: 1px solid #333333; border-left: 1px solid #333333;">1->2</td>
<td align=center style="padding: 8px; border-top: 1px solid #333333; border-left: 1px solid #333333;">2->3</td>
<td align=center style="padding: 8px; border-top: 1px solid #333333; border-left: 1px solid #333333;">3->4</td>
<td align=center style="padding: 8px; border-top: 1px solid #333333; border-left: 1px solid #333333;">4->5</td>
<td align=center style="padding: 8px; border-top: 1px solid #333333; border-left: 1px solid #333333;">5->6</td>
<td align=center style="padding: 8px; border-top: 1px solid #333333; border-left: 1px solid #333333;">6->7</td>
<td align=center style="padding: 8px; border-top: 1px solid #333333; border-left: 1px solid #333333;">7->8</td>
<td align=center style="padding: 8px; border-top: 1px solid #333333; border-left: 1px solid #333333;">8->8.1</td>
</tr>
<? $count=0; for ($age=15; $age<=18; $age++) { $count++; ?>
<tr bgcolor=<?=$color[$count%2]?>>
<td bgcolor=#99ff99 style="padding: 8px; border-top: 1px solid #333333; border-left: 1px solid #333333;"><?=$age?> ani</td>
<td align=center style="padding: 8px; border-top: 1px solid #333333; border-left: 1px solid #333333;"><?=$skill_speed['1'][$age]?></td>
<td align=center style="padding: 8px; border-top: 1px solid #333333; border-left: 1px solid #333333;"><?=$skill_speed['2'][$age]?></td>
<td align=center style="padding: 8px; border-top: 1px solid #333333; border-left: 1px solid #333333;"><?=$skill_speed['3'][$age]?></td>
<td align=center style="padding: 8px; border-top: 1px solid #333333; border-left: 1px solid #333333;"><?=$skill_speed['4'][$age]?></td>
<td align=center style="padding: 8px; border-top: 1px solid #333333; border-left: 1px solid #333333;"><?=$skill_speed['5'][$age]?></td>
<td align=center style="padding: 8px; border-top: 1px solid #333333; border-left: 1px solid #333333;"><?=$skill_speed['6'][$age]?></td>
<td align=center style="padding: 8px; border-top: 1px solid #333333; border-left: 1px solid #333333;"><?=$skill_speed['7'][$age]?></td>
<td align=center style="padding: 8px; border-top: 1px solid #333333; border-left: 1px solid #333333;"><?=$skill_speed['8'][$age]?></td>
<td align=center style="padding: 8px; border-top: 1px solid #333333; border-left: 1px solid #333333;"><?=$skill_speed['8.1'][$age]?></td>
</tr>
<? } ?>
</table><br><br><br>
<? }
}
?>
