<table class="contentpaneopen">
<tr>
	<td class="contentheading" width="100%">Predictor U20</td>
</tr>
</table>

<?php
$days = intval($_POST['days']);
$week = intval($_POST['week']);	
$submitted = intval($_POST['submitted']);

if($submitted == 1)
{
	if ( $days < 0 || $days > 111 )
		$message = '<b style="color: red;">Câmpul "vârsta (zile)" trebuie să cuprindă numere între 0 şi 111</b>'."\n";
	elseif ( $week < 1 || $week > 14 )
		$message .= '<b style="color: red;">Câmpul "Extras în săptămâna" trebuie să cuprindă numere între 1 şi 14</b>';
	else
	{
		$chances = array('Ideal', 'Şanse medii', 'Şanse mici', 'Şanse zero', );
		if ( $week >= 1 && $week <= 5 )
		{
			if ( $days >= 0 && $days <= 34 )
				$state = $chances[0];
			elseif ( $days >= 35 && $days <= 70 )
				$state = $chances[1];
			elseif ( $days >= 71 && $days <= 111 )
				$state = $chances[2];
		}
		elseif ( $week >= 6 && $week <= 10 )
		{
			if ( $days >= 0 && $days <= 34 )
				$state = $chances[1];
			elseif ( $days >= 35 && $days <= 70 )
				$state = $chances[2];
			elseif ( $days >= 71 && $days <= 111 )
				$state = $chances[1];
		}
		elseif ( $week >= 11 && $week <= 14 )
		{
			if ( $days >= 0 && $days <= 34 )
				$state = $chances[1];
			elseif ( $days >= 35 && $days <= 70 )
				$state = $chances[1];
			elseif ( $days >= 71 && $days <= 111 )
				$state = $chances[3];
		}
		
		$message = '<span style="color: green;">'.$state.'</span>';
	}
}
?>
<form action="<?php echo($_SERVER['PHP_SELF']); ?>?option=com_php&amp;Itemid=<?php echo($_GET['Itemid']); ?>" method="post"><div>
<input type="hidden" name="submitted" value="1">
<table>
	<tr>
		<td class="label"><label for="days">Vârsta (ani):</label></td>
		<td><input type="text" id="days" name="days" value="17" disabled="disabled"></td>
	</tr>
	<tr>
		<td class="label"><label for="days">Vârsta (zile):</label></td>
		<td><input type="text" id="days" name="days" value="<?php echo($days); ?>"></td>
	</tr>
	<tr>
		<td class="label"><label for="days">Extras în săptămâna:</label></td>
		<td><input type="text" id="days" name="week" value="<?php echo($week); ?>"></td>
	</tr>
	<tr>
		<td></td><td><?php echo($message); ?></td></td>
	</tr>	
	<tr>
		<td></td><td><input type="submit" value="estimează"></td>
	</tr>
</table>
</div></form>

