<?

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set("display_errors","1");

$useDelta=0;
$antrenor=$_GET['antrenor'];
$incredere=$_GET['incredere'];
$atitudine=$_GET['atitudine'];
$locatie=$_GET['locatie'];
$spirit=$_GET['spirit'];
$tactica=$_GET['tactica'];
$pozitie=str_replace('_', ' ', $_GET['pozitie']);

$calitati[0][0]=$_GET['q0'];
$calitati[1][0]=$_GET['q1'];
$calitati[2][0]=$_GET['q2'];
$calitati[3][0]=$_GET['q3'];
$calitati[4][0]=$_GET['q4'];
$calitati[5][0]=$_GET['q5'];

$calitati[6][0]=$_GET['q6']; //stamina
$calitati[7][0]=$_GET['q7']; //forma
$calitati[8][0]=$_GET['q8']; //xp



//preinitializarea constantelor
$positions[]='central_def';
$positions[]='side_def';
$positions[]='midfield';
$positions[]='central_att';
$positions[]='side_att';
$base_delta=array(1, 1, 1, 1, 1);
$base_multiplier=array(0.25, 0.25, 0.25, 0.25, 0.25);
$base_confidence=array(0, 0, 0, 0.0525, 0.0525);
$base_coach['def']=array(1.196307, 1.197332, 1, 0.92793, 0.921278);
$base_coach['neutral']=array(1.05, 1.05, 1, 1.05, 1.05);
$base_coach['off']=array(0.928162, 0.927577, 1, 1.135257, 1.133359);
$base_at['MOS']=array(1, 1, 1.10965, 1, 1);
$base_at['PIC']=array(1, 1, 0.839949, 1, 1);
$base_at['normal']=array(1, 1, 1, 1, 1);
$base_loc['home']=array(1, 1, 1.199529, 1, 1);
$base_loc['away']=array(1, 1, 1, 1, 1);
$base_loc['derby']=array(1, 1, 1.113699, 1, 1);
$base_tsPower=array(0, 0, 0.417779, 0, 0);
$base_tsPreMulti=array(1, 1, 0.147832, 1, 1);
$base_tactic['normal']=array(1, 1, 1, 1, 1);
$base_tactic['aim']=array(1, 0.853911, 1, 1, 1);
$base_tactic['aow']=array(0.858029, 1, 1, 1, 1);
$base_tactic['ca']=array(1, 1, 0.93, 1, 1);
$base_tactic['pc']=array(0.930999, 0.930663, 1, 1, 1);
$base_tactic['press']=array(1, 1, 1, 1, 1);
$base_tactic['ls']=array(1, 1, 0.950323, 0.970577, 0.97298);
$base_cubeMod=array(-0.000017, -0.000029, -0.000027, -0.000029, -0.000027);
$base_squareMod=array(0.008462, 0.011591, 0.008504, 0.011339, 0.012093);

//aportul calitatilor in functie de pozitie
$aport["K"]["central_def"]=array(0.568918, 0.264934, 0, 0, 0, 0);
$aport["K"]["side_def"]=array(0.629629, 0.276579, 0, 0, 0, 0);
$aport["CD"]["central_def"]=array(0, 0.602083, 0, 0, 0, 0);
$aport["CD"]["side_def"]=array(0, 0.499476, 0, 0, 0, 0);
$aport["CD"]["midfield"]=array(0, 0, 0.12596, 0, 0, 0);
$aport["CD off"]["central_def"]=array(0, 0.43655, 0, 0, 0, 0);
$aport["CD off"]["side_def"]=array(0, 0.365733, 0, 0, 0, 0);
$aport["CD off"]["midfield"]=array(0, 0, 0.169827, 0, 0, 0);
$aport["CD tw"]["central_def"]=array(0, 0.468513, 0, 0, 0, 0);
$aport["CD tw"]["side_def"]=array(0, 0.656592, 0, 0, 0, 0);
$aport["CD tw"]["midfield"]=array(0, 0, 0.088027, 0, 0, 0);
$aport["CD tw"]["side_att"]=array(0, 0, 0, 0.213672, 0, 0);
$aport["WB"]["central_def"]=array(0, 0.280401, 0, 0, 0, 0);
$aport["WB"]["side_def"]=array(0, 0.921337, 0, 0, 0, 0);
$aport["WB"]["midfield"]=array(0, 0, 0.078229, 0, 0, 0);
$aport["WB"]["side_att"]=array(0, 0, 0, 0.399755, 0, 0);
$aport["WB def"]["central_def"]=array(0, 0.314461, 0, 0, 0, 0);
$aport["WB def"]["side_def"]=array(0, 1.002422, 0, 0, 0, 0);
$aport["WB def"]["midfield"]=array(0, 0, 0.030981, 0, 0, 0);
$aport["WB def"]["side_att"]=array(0, 0, 0, 0.255317, 0, 0);
$aport["WB off"]["central_def"]=array(0, 0.238437, 0, 0, 0, 0);
$aport["WB off"]["side_def"]=array(0, 0.69943, 0, 0, 0, 0);
$aport["WB off"]["midfield"]=array(0, 0, 0.107886, 0, 0, 0);
$aport["WB off"]["side_att"]=array(0, 0, 0, 0.487884, 0, 0);
$aport["WB tm"]["central_def"]=array(0, 0.426132, 0, 0, 0, 0);
$aport["WB tm"]["side_def"]=array(0, 0.688952, 0, 0, 0, 0);
$aport["WB tm"]["midfield"]=array(0, 0, 0.078229, 0, 0, 0);
$aport["WB tm"]["side_att"]=array(0, 0, 0, 0.255317, 0, 0);
$aport["IM"]["midfield"]=array(0, 0, 0.468248, 0, 0, 0);
$aport["IM"]["central_def"]=array(0, 0.24966, 0, 0, 0, 0);
$aport["IM"]["side_def"]=array(0, 0.189319, 0, 0, 0, 0);
$aport["IM"]["central_att"]=array(0, 0, 0, 0, 0.193137, 0);
$aport["IM"]["side_att"]=array(0, 0, 0, 0, 0.189444, 0);
$aport["IM def"]["midfield"]=array(0, 0, 0.442128, 0, 0, 0);
$aport["IM def"]["central_def"]=array(0, 0.3705, 0, 0, 0, 0);
$aport["IM def"]["side_def"]=array(0, 0.27115, 0, 0, 0, 0);
$aport["IM def"]["central_att"]=array(0, 0, 0, 0, 0.130214, 0);
$aport["IM def"]["side_att"]=array(0, 0, 0, 0, 0.121811, 0);
$aport["IM off"]["midfield"]=array(0, 0, 0.442128, 0, 0, 0);
$aport["IM off"]["central_def"]=array(0, 0.134608, 0, 0, 0, 0);
$aport["IM off"]["side_def"]=array(0, 0.102543, 0, 0, 0, 0);
$aport["IM off"]["central_att"]=array(0, 0, 0, 0, 0.287063, 0);
$aport["IM off"]["side_att"]=array(0, 0, 0, 0, 0.187746, 0);
$aport["IM tw"]["midfield"]=array(0, 0, 0.412406, 0, 0, 0);
$aport["IM tw"]["central_def"]=array(0, 0.217029, 0, 0, 0, 0);
$aport["IM tw"]["side_def"]=array(0, 0.249179, 0, 0, 0, 0);
$aport["IM tw"]["central_att"]=array(0, 0, 0, 0, 0.134745, 0);
$aport["IM tw"]["side_att"]=array(0, 0, 0, 0.428863, 0.235698, 0);
$aport["WI"]["midfield"]=array(0, 0, 0.242848, 0, 0, 0);
$aport["WI"]["central_def"]=array(0, 0.125486, 0, 0, 0, 0);
$aport["WI"]["side_def"]=array(0, 0.349951, 0, 0, 0, 0);
$aport["WI"]["central_att"]=array(0, 0, 0, 0, 0.061538, 0);
$aport["WI"]["side_att"]=array(0, 0, 0, 0.674398, 0.182107, 0);
$aport["WI def"]["midfield"]=array(0, 0, 0.203662, 0, 0, 0);
$aport["WI def"]["central_def"]=array(0, 0.156868, 0, 0, 0, 0);
$aport["WI def"]["side_def"]=array(0, 0.463477, 0, 0, 0, 0);
$aport["WI def"]["central_att"]=array(0, 0, 0, 0, 0.031035, 0);
$aport["WI def"]["side_att"]=array(0, 0, 0, 0.571036, 0.15016, 0);
$aport["WI off"]["midfield"]=array(0, 0, 0.203662, 0, 0, 0);
$aport["WI off"]["central_def"]=array(0, 0.050401, 0, 0, 0, 0);
$aport["WI off"]["side_def"]=array(0, 0.172133, 0, 0, 0, 0);
$aport["WI off"]["central_att"]=array(0, 0, 0, 0, 0.080314, 0);
$aport["WI off"]["side_att"]=array(0, 0, 0, 0.789342, 0.213452, 0);
$aport["WI tm"]["midfield"]=array(0, 0, 0.306594, 0, 0, 0);
$aport["WI tm"]["central_def"]=array(0, 0.152469, 0, 0, 0, 0);
$aport["WI tm"]["side_def"]=array(0, 0.284824, 0, 0, 0, 0);
$aport["WI tm"]["central_att"]=array(0, 0, 0, 0, 0.08768, 0);
$aport["WI tm"]["side_att"]=array(0, 0, 0, 0.445048, 0.115254, 0);
$aport["FW"]["central_att"]=array(0, 0, 0, 0, 0.207589, 0.563149);
$aport["FW"]["side_att"]=array(0, 0, 0, 0.142456, 0.10029, 0.184292);
$aport["FW tw"]["central_att"]=array(0, 0, 0, 0, 0.146863, 0.342343);
$aport["FW tw"]["side_att"]=array(0, 0, 0, 0.392185, 0.148148, 0.371022);
$aport["FW def"]["midfield"]=array(0, 0, 0.217011, 0, 0, 0);
$aport["FW def"]["central_att"]=array(0, 0, 0, 0, 0.322422, 0.346607);
$aport["FW def"]["side_att"]=array(0, 0, 0, 0.097975, 0.186512, 0.095023);
$aport["TDF"]["midfield"]=array(0, 0, 0.217011, 0, 0, 0);
$aport["TDF"]["central_att"]=array(0, 0, 0, 0, 0.470666, 0.346607);
$aport["TDF"]["side_att"]=array(0, 0, 0, 0.097975, 0.220424, 0.095023);


$calitati[6][1]=$calitati[6][0];
$calitati[6][2]=$calitati[6][1]+6.5;
if ($calitati[6][2]<7) $calitati[6][3]=7;
else $calitati[6][3]=$calitati[6][2];
if ($calitati[6][3]>15.25) $calitati[6][4]=15.25;
else $calitati[6][4]=$calitati[6][3];
$calitati[6][5]=$calitati[6][4]/14;
$calitati[6][6]=pow($calitati[6][5], 0.6);
$calitati[6][7]=$calitati[6][6]*1;
$calitati[6][8]=$calitati[6][7]+0;

$calitati[7][1]=$calitati[7][0];
$calitati[7][2]=$calitati[7][1]-0.6;
if ($calitati[7][2]<0) $calitati[7][3]=0;
else $calitati[7][3]=$calitati[7][2];
if ($calitati[7][3]>7) $calitati[7][4]=7;
else $calitati[7][4]=$calitati[7][3];
$calitati[7][5]=$calitati[7][4]/7;
$calitati[7][6]=pow($calitati[7][5], 0.45);
$calitati[7][7]=$calitati[7][6]*1;
$calitati[7][8]=$calitati[7][7]+0;

$calitati[8][1]=$calitati[8][0];
$calitati[8][2]=$calitati[8][1]-0.5;
if ($calitati[8][2]<0) $calitati[8][3]=0;
else $calitati[8][3]=$calitati[8][2];
$calitati[8][4]=$calitati[8][3];
$calitati[8][5]=$calitati[8][4]*1;
$calitati[8][6]=pow($calitati[8][5], 0.5);
$calitati[8][7]=$calitati[8][6]*0.0716;
$calitati[8][8]=$calitati[8][7]+1;

$calitati_product=$calitati[6][8]*$calitati[7][8]*$calitati[8][8];


for ($i=0; $i<=5; $i++)
{
if ($calitati[$i][0]<8) $calitati[$i][1]=$calitati[$i][0]+0.5;
else $calitati[$i][1]=$calitati[$i][0];
$calitati[$i][2]=$calitati[$i][1]-1;
if ($calitati[$i][2]<0) $calitati[$i][3]=0;
else $calitati[$i][3]=$calitati[$i][2];
$calitati[$i][4]=$calitati[$i][3];
$calitati[$i][5]=$calitati[$i][4]*$calitati_product;
}


foreach ($positions as $key=>$pos)
{
$rating[$pos]=0;
for ($i=0; $i<=5; $i++)
{
$rating[$pos]+=$aport[$pozitie][$pos][$i]*$calitati[$i][5];
}

$squareMod[$pos]=$rating[$pos]+($base_squareMod[$key]*pow($rating[$pos], 2));

$cubeMod[$pos]=$squareMod[$pos]+($base_cubeMod[$key]*pow($squareMod[$pos], 3));

$tactic[$pos]=$cubeMod[$pos]*$base_tactic[$tactica][$key];

$teamspirit[$pos]=$tactic[$pos]*pow($spirit*$base_tsPreMulti[$key], $base_tsPower[$key]);

$location[$pos]=$teamspirit[$pos]*$base_loc[$locatie][$key];

$attitude[$pos]=$location[$pos]*$base_at[$atitudine][$key];

$coach[$pos]=$attitude[$pos]*$base_coach[$antrenor][$key];

$confidence[$pos]=$coach[$pos]*(1+$base_confidence[$key]*($incredere-5));

$multiplier[$pos]=$confidence[$pos]*$base_multiplier[$key];

$final[$pos]=$multiplier[$pos];
if (useDelta==1) $final[$pos]+=$base_delta[$key];
}

header("Content-type: text/xml");
print '<?xml version="1.0"?>
<ratinguri>';

foreach ($positions as $pos)
print "<rating>+".round($final[$pos],6)."</rating>";
?></ratinguri>
