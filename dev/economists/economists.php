<?php
error_reporting(E_NONE); // warnings or notices makes AJAX feel bad :)
//include needed files
require_once('common.php');

//get data from the user
$userInfo = array();
$userInfo['finances'] = str2int($_GET['finances']);

//check to see where the user finances fit
foreach($economists_table as $current){
	if(($userInfo['finances']>=str2int($current['min_cash']))&&($userInfo['finances']<=str2int($current['max_cash']))){
		$economist_array=$current;
		break;
	}
}

require_once('economists.inc.html');
?>
