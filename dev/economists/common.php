<?php
$economists_table = array(
0 => array('min_cash' => '0', 'max_cash' => '101.333.333', 'economists' => '0', 'min_income' => '0', 'max_income' => '0', 'wages' => '0', 'min_profit' => '0', 'max_profit' => '0'),
1 => array('min_cash' => '101.333.334', 'max_cash' => '233.816.642', 'economists' => '1', 'min_income' => '38.000', 'max_income' => '86.000', 'wages' => '36.000', 'min_profit' => '2.000', 'max_profit' => '50.000' ),
2 => array('min_cash' => '233.816.643', 'max_cash' => '304.840.942', 'economists' => '2', 'min_income' => '124.000', 'max_income' => '160.000', 'wages' => '72.000', 'min_profit' => '52.000', 'max_profit' => '88.000' ),
3 => array('min_cash' => '304.840.943', 'max_cash' => '360.000.000', 'economists' => '3', 'min_income' => '198.000', 'max_income' => '232.000', 'wages' => '108.000', 'min_profit' => '90.000', 'max_profit' => '124.000' ),
4 => array('min_cash' => '360.000.001', 'max_cash' => '412.629.077', 'economists' => '4', 'min_income' => '270.000', 'max_income' => '308.000', 'wages' => '144.000', 'min_profit' => '126.000', 'max_profit' => '164.000' ),
5 => array('min_cash' => '412.629.078', 'max_cash' => '452.883.436', 'economists' => '5', 'min_income' => '346.000', 'max_income' => '378.000', 'wages' => '180.000', 'min_profit' => '166.000', 'max_profit' => '198.000' ),
6 => array('min_cash' => '452.883.437', 'max_cash' => '491.857.767', 'economists' => '6', 'min_income' => '416.000', 'max_income' => '450.000', 'wages' => '216.000', 'min_profit' => '200.000', 'max_profit' => '234.000' ),
7 => array('min_cash' => '491.857.768', 'max_cash' => '507.316.542', 'economists' => '7', 'min_income' => '488.000', 'max_income' => '500.000', 'wages' => '252.000', 'min_profit' => '236.000', 'max_profit' => '248.000' ),
8 => array('min_cash' => '507.316.543', 'max_cash' => '555.737.428', 'economists' => '6', 'min_income' => '466.000', 'max_income' => '500.000', 'wages' => '216.000', 'min_profit' => '250.000', 'max_profit' => '284.000' ),
9 => array('min_cash' => '555.737.429', 'max_cash' => '621.333.333', 'economists' => '5', 'min_income' => '466.000', 'max_income' => '500.000', 'wages' => '180.000', 'min_profit' => '286.000', 'max_profit' => '320.000' ),
10 => array('min_cash' => '621.333.334', 'max_cash' => '717.453.934', 'economists' => '4', 'min_income' => '466.000', 'max_income' => '500.000', 'wages' => '144.000', 'min_profit' => '322.000', 'max_profit' => '356.000' ),
11 => array('min_cash' => '717.453.935', 'max_cash' => '878.698.026', 'economists' => '3', 'min_income' => '466.000', 'max_income' => '500.000', 'wages' => '108.000', 'min_profit' => '358.000', 'max_profit' => '392.000' ),
12 => array('min_cash' => '878.698.027', 'max_cash' => '1.242.666.666', 'economists' => '2', 'min_income' => '466.000', 'max_income' => '500.000', 'wages' => '72.000', 'min_profit' => '394.000', 'max_profit' => '428.000'),
13 => array('min_cash' => '1.242.666.667', 'max_cash' => '9.999.999.999', 'economists' => '1', 'min_income' => '466.000', 'max_income' => '500.000', 'wages' => '36.000', 'min_profit' => '430.000', 'max_profit' => '464.000' )
);

//converts string to integer
function str2int($string, $concat = true) {
    $length = strlen($string);    
    for ($i = 0, $int = '', $concat_flag = true; $i < $length; $i++) {
        if (is_numeric($string[$i]) && $concat_flag) {
            $int .= $string[$i];
        } elseif(!$concat && $concat_flag && strlen($int) > 0) {
            $concat_flag = false;
        }        
    }
    
    return (int) $int;
}
?>
