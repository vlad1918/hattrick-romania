<?php
/**
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 *
 * Joomla template by Vlad DIMA
 */

defined('_JEXEC') or die('Restricted access');

$view = $_GET['view'];
if ($view == 'category')
	$_SESSION['$categId'] = $_GET['id'];

if(! preg_match('/MSIE 6/', $_SERVER['HTTP_USER_AGENT']))  // MSIE 6 freaks out if a string is inserted before the DOCTYPE
	echo ('<?xml version="1.0" encoding="utf-8"?'.'>'."\n");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>" >
<head>
	<jdoc:include type="head" />
	<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/hattrick/css/style.css" type="text/css" />
	<?php if ($_SESSION['$categId'] == 4) { // include a special CSS for announcements  ?>
		<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/hattrick/css/announcements.css" type="text/css" />
	<?php } ?>
</head>
<body>
<div id="container">
<div id="header">
	<a id="logo" href="."></a>
	<div id="banner">
		<div id="leaderboard">
			<div id="googleAdsense">
				<script type="text/javascript"><!--
				google_ad_client = "pub-0098964019585080";
				/* leaderboard2 */
				google_ad_slot = "7782232005";
				google_ad_width = 728;
				google_ad_height = 90;
				//-->
				</script>
				<script type="text/javascript"
				src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
				</script>
			</div>
			<!--div id="bannerBar">
				<img src="<?php echo $this->baseurl ?>/templates/hattrick-decembrie/images/bannerBar.png" alt="Primul club Hattrick în Galați" width="728" width="90">
			</div>
			<!--script type="text/javascript">
			<!--
			//Hide either #googleAdsense or #bannerBar
			var random = Math.random();
			if (random > 0.6)
			{
				document.getElementById('googleAdsense').style.display='none';
			}
			else
			{
				document.getElementById('bannerBar').style.display='none';
			}			
			-->
			</script-->
		</div>
		<a id="adExplanation" href="http://hattrick-romania.ro/acasa/71-ce-e-cu-reclamele-astea.html">Ce e cu reclamele astea?</a>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
	<div id="topmenu">
		<jdoc:include type="modules" name="top" />
		<jdoc:include type="modules" name="search" />
		<div class="clear"></div>
	</div>
</div><!-- #header -->
<div id="body">
	<div id="leftmenu">
	<?php
	/** The left menu box varies depending on what category the user has selected from the top menu **/
	switch($_SESSION['$categId'])
	{
		case 6:
			echo ('<jdoc:include type="modules" name="utile" style="xhtml" />');
		break;
		case 2:
			echo ('<jdoc:include type="modules" name="nationale" style="xhtml" />');
		break;
		case 3:
			echo ('<jdoc:include type="modules" name="comunitate" style="xhtml" />');
		break;
		case 4:
			echo ('<jdoc:include type="modules" name="htpress" style="xhtml" />');
		break;			
		case 5:
			echo ('<jdoc:include type="modules" name="moldova" style="xhtml" />');
		break;
		case 8:
			echo ('<jdoc:include type="modules" name="masters" style="xhtml" />');
		break;
		default:
		case 1:
			echo ('<jdoc:include type="modules" name="unelte" style="xhtml" />');
		break;	
	}
	?>
	<jdoc:include type="modules" name="poll" style="xhtml" />
	<div class="moduletable">
		<h3>Reclame</h3>
		<div style="margin: 5px auto; width: 137px;">
			<script type="text/javascript"><!--
			google_ad_client = "pub-0098964019585080";
			/* vertical2 */
			google_ad_slot = "7060914262";
			google_ad_width = 120;
			google_ad_height = 240;
			//-->
			</script>
			<script type="text/javascript"
			src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
			</script>
		</div>
	</div>
	<jdoc:include type="modules" name="other1" style="xhtml" />
	<jdoc:include type="modules" name="other2" style="xhtml" />
	<jdoc:include type="modules" name="other3" style="xhtml" />
	</div>
	<div id="main">
		<div class="content">				
			<jdoc:include type="component" />
		</div>
		<div class="end"></div>
	</div>
	<div style="clear: left;"></div>
</div><!-- #body -->
<div id="footer">
</div><!-- #footer -->
</div><!-- #container -->
<!-- Google Analytics -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-12199610-1");
pageTracker._trackPageview();
} catch(err) {}
</script>
</body>
</html>

